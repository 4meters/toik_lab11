package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.dto.UpdateMovieDto;
import com.demo.springboot.model.MovieRepository;
import com.demo.springboot.model.MovieRepositoryImpl;
import com.demo.springboot.model.MovieService;
import com.demo.springboot.model.MovieServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@CrossOrigin
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    //private final MovieListDto movies;
    @Autowired
    private MovieService movieService;

    public MovieApiController() {
        movieService=new MovieServiceImpl();
        //MovieRepository movieRepository=new MovieRepositoryImpl();
        //List<MovieDto> moviesList = new ArrayList<>();
        //moviesList.add(new MovieDto(1,
         //       "Piraci z Krzemowej Doliny",
           //     1999,
             //   "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        //);
        //movies = new MovieListDto(moviesList);
    }

    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        //LOG.info("--- get all movies: {}", movieServiceImpl.getMovieRepository());
        return ResponseEntity.ok().body(movieService.getMovieRepository().getMovieListDto());    // = new ResponseEntity<>(movies, HttpStatus.OK);
    }


}
