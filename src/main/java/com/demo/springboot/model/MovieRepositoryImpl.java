package com.demo.springboot.model;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MovieRepositoryImpl implements MovieRepository {
    private MovieListDto movieListDto;
    //private List<MovieDto> movies;

    public MovieRepositoryImpl(){
        this.movieListDto=new MovieListDto(sampleMovieList());
    }
    public MovieRepositoryImpl(MovieListDto movieListDto) {
        this.movieListDto=movieListDto;
    }
    public List<MovieDto> sampleMovieList(){
        List<MovieDto> moviesList= new ArrayList<>();
        moviesList.add(new MovieDto(1,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        moviesList.add(new MovieDto(2,
                "Ja, robot",
                2004,
                "https://fwcdn.pl/fpo/54/92/95492/7521206.6.jpg")
        );
        moviesList.add(new MovieDto(3,
                "Kod nieśmiertelności",
                2011,
                "https://fwcdn.pl/fpo/89/67/418967/7370853.6.jpg")
        );
        moviesList.add(new MovieDto(4,
                "Ex Machina",
                2015,
                "https://fwcdn.pl/fpo/64/19/686419/7688121.6.jpg")
        );
        return moviesList;
    }

    public MovieListDto getMovieListDto() {
        return movieListDto;
    }
}
